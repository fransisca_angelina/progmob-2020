package ukdw.com.progmob_2020.Dosen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Crud.MahasiswaAddActivity;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class DosenAddActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_add);

        final EditText edNama = (EditText) findViewById(R.id.editTextNama);
        final EditText edNidn = (EditText) findViewById(R.id.editTextNidn);
        final EditText edAlamat = (EditText) findViewById(R.id.editTextAlamat);
        final EditText edEmail = (EditText) findViewById(R.id.editTextEmail);
        Button btnSimpan = (Button) findViewById(R.id.buttonSimpanDsn);
        pd = new ProgressDialog(DosenAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        edNama.getText().toString(),
                        edNidn.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        "Kosongkan Saja Di Isi Sembarang, Karena Di Random Sistem",
                        "72180185" //DI ISI NIM
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this, "DATA BERHASIL DI SIMPAN", Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this, "GAGAL", Toast.LENGTH_LONG);
                    }
                });
            }
        });
    }
}