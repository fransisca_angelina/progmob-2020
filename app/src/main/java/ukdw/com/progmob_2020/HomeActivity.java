package ukdw.com.progmob_2020;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import ukdw.com.progmob_2020.Crud.MainMhsActivity;
import ukdw.com.progmob_2020.Dosen.MainDsnActivity;
import ukdw.com.progmob_2020.Model.User;

public class HomeActivity extends AppCompatActivity {

    ProgressDialog pd;
    List<User> users;
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //variabel
        ImageView imgMhs = (ImageView) findViewById(R.id.imgMhs);
        ImageView imgDsn = (ImageView) findViewById(R.id.imgDosen);
        ImageView imgMk = (ImageView) findViewById(R.id.imgMatkul);
        ImageView imgLogout = (ImageView) findViewById(R.id.imgLogout);

        //action
        imgMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });

        imgDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainDsnActivity.class);
                startActivity(intent);
            }
        });

    }
}
