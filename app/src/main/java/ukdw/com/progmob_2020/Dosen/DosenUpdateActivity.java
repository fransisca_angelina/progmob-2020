package ukdw.com.progmob_2020.Dosen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Crud.MahasiswaUpdateActivity;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class DosenUpdateActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        final EditText edNidnCari = (EditText) findViewById(R.id.editTextNidnCari);
        final EditText edNamaBaru = (EditText) findViewById(R.id.editTextNama2);
        final EditText edNidnBaru = (EditText) findViewById(R.id.editTextNidn2);
        final EditText edAlamatBaru = (EditText) findViewById(R.id.editTextAlamat2);
        final EditText edEmailBaru = (EditText) findViewById(R.id.editTextEmail2);
        Button btnUpdate = (Button) findViewById(R.id.buttonUbahMhs);
        pd = new ProgressDialog(DosenUpdateActivity.this);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        edNidnCari.getText().toString(),
                        edNamaBaru.getText().toString(),
                        edNidnBaru.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        "Kosongkan Saja Di Isi Sembarang, Karena Di Random Sistem",
                        "72180185" //DI ISI NIM
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "DATA BERHASIL DI UBAH", Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "GAGAL", Toast.LENGTH_LONG);
                    }
                });
            }
        });
    }
}